package edu.uprm.cse.datastructures.cardealer;



import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
//import edu.uprm.cse.datastructures.cardealer.model.CarComparator;
//import edu.uprm.cse.datastructures.cardealer.util.CSDLinkedList;
import edu.uprm.cse.datastructures.cardealer.util.CarList;
import edu.uprm.cse.datastructures.cardealer.util.SortedList;

@Path("/cars")
public class CarManager {
	
    //gets the car list.
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars() {
		SortedList<Car> list = CarList.getInstance();
		Car[] cararr = new Car[list.size()];
		for(int i = 0; i < cararr.length; i++) {
			cararr[i] = list.get(i);
		}
		return cararr;
	}
	//gets a specific car based on its id number.
	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") long id) throws NotFoundException {
		SortedList<Car> list = CarList.getInstance();
		for(int i = 0; i < list.size(); i++) {
			if(id == list.get(i).getCarId()) {
				return list.get(i);
			}
		}
		throw new WebApplicationException(404);
	}
	//adds a new car to the list. 
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car car) {
		SortedList<Car> list = CarList.getInstance();
		for(int i = 0;i<list.size();i++)
		{
			if(car.getCarId()==list.get(i).getCarId())
			{
				return Response.status(Response.Status.CONFLICT).build();
			}
		}
		list.add(car);
		return Response.status(201).build();
	}
	//it changes some values on a Car. (pricing, amongst other values).
	@PUT
	@Path("{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updatePerson(Car car) {
		SortedList<Car> list = CarList.getInstance();
		for (int i = 0; i < list.size(); i++) {
			if (car.getCarId() == list.get(i).getCarId()) {
				list.remove(list.get(i));
				list.add(car);
				return Response.status(Response.Status.OK).build();
			}
		}
		return Response.status(Response.Status.NOT_FOUND).build();
	}
	//it removes a given car from the list.
	@DELETE
	@Path("{id}/delete")
	public Response deletePerson(@PathParam("id") long id) {
		SortedList<Car> list = CarList.getInstance();
		for (int i = 0; i < list.size(); i++) {
			if (id == list.get(i).getCarId()) {
				list.remove(list.get(i));
				return Response.status(Response.Status.OK).build();
			}
		}
//		throw new NotFoundException("Error"+ "Car " + id + " not found");
		return Response.status(Response.Status.NOT_FOUND).build();
	}
	

}
