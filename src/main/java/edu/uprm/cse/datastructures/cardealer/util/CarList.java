//package edu.uprm.cse.datastructures.cardealer.util;
//
//public class CarList {
//
//}
package edu.uprm.cse.datastructures.cardealer.util;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;

public class CarList {

	public static CSDLinkedList<Car> CarList = new CSDLinkedList<Car>( new CarComparator());
	
	public static CSDLinkedList<Car> getInstance(){
		return CarList;
		}
	public static void resetCars() {
		CarList = new CSDLinkedList<Car>(new CarComparator());
		}
}
