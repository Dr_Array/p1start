package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarComparator implements Comparator<Car> {

	@Override
	public int compare(Car car1, Car car2)
	{
		//checks if the objects being compared are indeed cars.
		if(!(car1 instanceof Car) && !(car2 instanceof Car))
		{
			throw new IllegalStateException("Object inst an instance of car.");
		}
		//checks if car1 is "smaller" than car2.
		if(car1.getCarBrand().compareTo(car2.getCarBrand())==0)
		{
			if(car1.getCarModel().compareTo(car2.getCarModel())==0)
			{
					return car1.getCarModelOption().compareTo(car2.getCarModelOption());
			}
			return car1.getCarModel().compareTo(car2.getCarModel());
		}
		return car1.getCarBrand().compareTo(car2.getCarBrand());
	}
	
	
	
	

}
