package edu.uprm.cse.datastructures.cardealer.util;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class CSDLinkedList<E> implements SortedList<E> {
	
	private class Node<E>{
		private E element;
		private Node<E> prev;
		private Node<E> next;
		public E getElement() {
			return element;
		}
		public void setElement(E element) {
			this.element = element;
		}
		public Node<E> getPrev() {
			return prev;
		}
		public void setPrev(Node<E> prev) {
			this.prev = prev;
		}
		public Node<E> getNext() {
			return next;
		}
		public void setNext(Node<E> next) {
			this.next = next;
		}
		
		
	}
	private int currentsize;
	private Node<E> header;
	private Comparator<E> comp;
	
	public CSDLinkedList(Comparator<E> c) {
		header = new Node<E>();
		currentsize = 0;
		
		this.header.setElement(null);
		this.header.setPrev(header);
		this.header.setNext(header);
		this.comp = c;
	}
	
	

	@Override
	public Iterator<E> iterator() {
		List<E> iterlist = new ArrayList<E>();
		Node<E> pos = this.header.getNext();
		while(pos != this.header) {
			iterlist.add(pos.getElement());
			pos = pos.getNext();
		}
		return iterlist.iterator();
	}

	@Override
	public boolean add(E obj) {
		Node<E> temp = new Node<E>();
		temp.setElement(obj);
		if(obj == null) {
			return false;
		}
		else {
			for(Node<E> position = this.header.getNext(); position != this.header; position = position.getNext()) {
				//This bit of code places obj in between another element if its smaller than any element.
				if(comp.compare(obj, position.getElement()) <= 0) {
					temp.setNext(position);
					temp.setPrev(position.getPrev());
					position.getPrev().setNext(temp);
					position.setPrev(temp);
					this.currentsize++;
					
					return true;
				}
			}
			//Because this list is sorted in ascending order, the biggest element gets placed last.
			temp.setPrev(this.header.getPrev());
			temp.setNext(this.header);
			this.header.setPrev(temp);
			temp.getPrev().setNext(temp);
			this.currentsize++;
			return true;
			
		}
	}

	@Override
	public int size() {
		return currentsize;
	}

	@Override
	public boolean remove(E obj) {
		if(obj == null) {
			throw new IllegalArgumentException();
		}
		else if(!this.contains(obj)) {
			return false;
		}
		else {
			//this while loop iterates all over the list to search for obj, to remove it.
			Node<E> temp = this.header.getNext();
			while(temp != this.header) {
				if(temp.getElement().equals(obj)) {
					//this runs if obj is at the end of the list.
					if(temp.getNext() == null) {
						temp.getPrev().setNext(temp.getNext());
						temp.setPrev(null);
						temp.setNext(null);
						temp.setElement(null);
						this.currentsize--;
						return true;
					}
					else {
						//this severs all connections obj has with the previous and net nodes, and 
						//decreases the list size.
						temp.getNext().setPrev(temp.getPrev());
						temp.getPrev().setNext(temp.getNext());
						this.currentsize--;
						return true;
					}
				}
				temp = temp.getNext();
			}
			return false;
		}
	}

	@Override
	public boolean remove(int index) {
		int pos = 0;
		Node<E> temp = null;
		if(index < 0 || index > this.size()) {
			throw new IndexOutOfBoundsException();
		}
		else if(this.isEmpty()) {
			return false;
		}
		else {
			//this iterates all over the list looking for the index, and then severs the connections
			//that obj has with the prev and next nodes.
			temp = this.header.getNext();
			while(pos < index) {
				temp = temp.getNext();
				pos++;
			}
			temp.getPrev().setNext(temp.getNext());
			this.currentsize--;
			temp.setElement(null);
			temp.setNext(null);

			return true;
		}
	}

	@Override
	public int removeAll(E obj) {
		int result = 0;
		while(this.remove(obj)) {
			result++;
		}
		return result;
	}

	@Override
	public E first() {
		return this.isEmpty() ? null : this.header.getNext().getElement();
	}

	@Override
	public E last() {
		
		return this.header.getPrev().getElement();
	}

	@Override
	public E get(int index) {
		if(index < 0 || index > this.size()) {
			throw new IndexOutOfBoundsException();
		}
		else {
			int pos = 0;
			Node<E> temp = this.header.getNext();
			while(pos < index) {
				temp = temp.getNext();
				pos++;
				
			}
			return temp.getElement();
		}
	}

	@Override
	public void clear() {
		while(!this.isEmpty()) {
			this.remove(0);
		}
		
	}

	@Override
	public boolean contains(E e) {
		return this.firstIndex(e) >= 0;
	}

	@Override
	public boolean isEmpty() {
		return currentsize == 0;
	}

	@Override
	public int firstIndex(E e) {
		int i = 0;
		//iterates over the list, looking for the first occurence of obj.  if the for finishes its loop,
		//then obj isnt on the list and -1 is returned.
		for (Node<E> temp = this.header.getNext(); temp.getElement() != null; 
				temp = temp.getNext(), i++) {
			if (temp.getElement().equals(e)) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public int lastIndex(E e) {
		int result = -1;
		if(e == null) {
			throw new IllegalArgumentException();
		}
		else {
			//this iterates over the list and updates the index if a new occurence
			//of obj is found.  It then returns the last time obj occurs in the list.
			Node<E> temp = this.header.getNext();
			int position = 0;
			while(temp != this.header) {
				if(temp.getElement().equals(e)) {
					result = position;
				}
				temp = temp.getNext();
				position++;
			}
			
			return result;
			
		}
	}

}
